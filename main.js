var keyboard = new THREEx.KeyboardState();

function main(){

	var scene = new THREE.Scene();
	var box = generateBox(1,1,1);
	box.name = "box-1";
	box.position.y = box.geometry.parameters.height/2;

	var floor = generateFloor(10, 50);
	floor.name = "floor";
	floor.rotation.x = Math.PI/2;

	var pointLight = generatePointLight(0xffffff, 1);
	pointLight.position.y = 5;
	pointLight.position.x = 2;


	floor.add(box);
	scene.add(floor);
	scene.add(pointLight);
	var moon = generateMoon();
	scene.add(moon);

	var camera = new THREE.PerspectiveCamera(
		45,
		window.innerWidth/window.innerHeight,
		1,
		1000
	);

	camera.position.x = 1;
	camera.position.y = 5;
	camera.position.z = 5;
	camera.lookAt(new THREE.Vector3(0,0,-5));

	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, innerHeight);
	renderer.setClearColor('rgb(60,60,60)');
	renderer.shadowMap.enabled = true;
	document.getElementById('webgl').appendChild(renderer.domElement);
	var controls = new THREE.OrbitControls(camera, renderer.domElement);
	update(renderer, scene, camera, controls);
	return scene;
}

function update(renderer, scene, camera, controls){
	renderer.render(scene, camera);
	scene.children[0].rotation.x += 0.01;
	var floor = scene.getObjectByName("floor");
	floor.rotation.z += 0.01;

	scene.traverse(function (child){
		// child.position.x += 0.01;
	});

	controls.update();

	var box = scene.getObjectByName("box-1");
	if(keyboard.pressed("A")){
		box.translateX(1);
	}
	if(keyboard.pressed("D")){
	box.translateX(-1);
	}
	requestAnimationFrame(function(){
		update(renderer, scene, camera, controls);
	});
}

function generateBox(w, h, d){
	var geo = new THREE.BoxGeometry(w, h, d);
	var mat = new THREE.MeshPhongMaterial({
	color:"rgb(100, 100, 100)",
	});
	var mesh = new THREE.Mesh(geo, mat);
	mesh.castShadow = true;
	return mesh;
}

function generateFloor(w, d){
	var geo = new THREE.PlaneGeometry(w, d);
	var mat = new THREE.MeshPhongMaterial({
	color:"rgb(100, 100, 100)",
	side: THREE.DoubleSide
	});
	var mesh = new THREE.Mesh(geo, mat);
	mesh.receiveShadow = true;
	return mesh;
}

function generatePointLight(color, intensity){
	var pointLight = new THREE.PointLight(color, intensity);
	pointLight.castShadow = true;
	return pointLight;
}

function generateMoon(){
	var sphere = new THREE.SphereGeometry(3, 42, 42);
	var moonTexture = new THREE.TextureLoader().load( 'moon.jpg' );
	var moonMat = new THREE.MeshBasicMaterial({map: moonTexture});
	var moon =  new THREE.Mesh(sphere, moonMat);

	moon.position.x = -15;
	moon.position.y = 15;
	moon.position.z = 15;

	return moon;
}
main();
